#!/usr/bin/env python3

# An application launcher which lets you type to search for applications and
# commands and presents the results in a list.
#
# Requirements: Python 3.6, PyQt5, PyXDG
# Install on Debian with: apt install python3-pyqt5 python3-xdg dex
#
# Copyright 2020-2023 Tomas Åkesson <tomas@entropic.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import glob
import os
import re
import sqlite3
import subprocess
import sys
from typing import Dict, List, Union

try:
    from xdg.DesktopEntry import DesktopEntry
except ImportError:
    print("xdg not found. Please install it.")
    sys.exit(1)

try:
    from PyQt5 import QtCore, QtGui, QtWidgets
    from PyQt5.QtCore import Qt, QTimer
    from PyQt5.QtWidgets import QWidget, QMainWindow
except ImportError:
    print("Qt5 not found. Please install it.")
    sys.exit(1)

DB_FILE: str = os.environ["HOME"]+"/.local/share/tlauncher/history.sqlite"

class Ui_MainWindow(object):
    def setupUi(self, mainWindow):
        mainWindow.setObjectName("mainWindow")
        mainWindow.resize(273, 240)
        self.centralwidget = QtWidgets.QWidget(mainWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.SearchInput = QtWidgets.QLineEdit(self.centralwidget)
        self.SearchInput.setObjectName("SearchInput")
        self.verticalLayout_2.addWidget(self.SearchInput)
        self.ResultsList = QtWidgets.QListWidget(self.centralwidget)
        self.ResultsList.setEditTriggers(QtWidgets.QAbstractItemView.AnyKeyPressed|QtWidgets.QAbstractItemView.DoubleClicked|QtWidgets.QAbstractItemView.SelectedClicked)
        self.ResultsList.setObjectName("ResultsList")
        self.verticalLayout_2.addWidget(self.ResultsList)
        mainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(mainWindow)
        QtCore.QMetaObject.connectSlotsByName(mainWindow)

    def retranslateUi(self, mainWindow):
        _translate = QtCore.QCoreApplication.translate
        mainWindow.setWindowTitle(_translate("mainWindow", "Run Application"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent=parent)
        self.setupUi(self)
        self.SearchInput.returnPressed.connect(self.launchSelected)
        self.SearchInput.textChanged['QString'].connect(self.on_text_changed)
        self.ResultsList.itemActivated['QListWidgetItem*'].connect(self.launchSelected)
        self.ResultsList.itemClicked['QListWidgetItem*'].connect(self.launchSelected)
        self.results: List = []
        self.max_results_to_show: int = 20

        homedir: str = os.environ['HOME']
        self.desktop_file_globs = [
            f'{homedir}/.local/share/applications/*.desktop',
            '/usr/share/applications/*.desktop',
            '/usr/local/share/applications/*.desktop',
            '/usr/share/applications/kde4/*.desktop',
        ]

        self.path_list: List = os.environ["PATH"].split(os.pathsep)
        self.score_cache: Dict = {}
        self.icon_cache: Dict = {}
        self.item_cache: Dict = {}

        # Use a timer to prevent updating the diff on every textChanged event
        self.timer: QTimer = QTimer(self)
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.update_results_list)


    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self.close()

        elif event.key() == QtCore.Qt.Key_Down and self.SearchInput.hasFocus():
            self.ResultsList.setFocus()
            self.ResultsList.setCurrentRow(self.ResultsList.currentRow() + 1)

        elif event.key() == QtCore.Qt.Key_PageDown and self.SearchInput.hasFocus():
            self.ResultsList.setFocus()
            self.ResultsList.setCurrentRow(self.ResultsList.currentRow() + 10)

        elif event.key() == QtCore.Qt.Key_Down:
            self.ResultsList.setFocus()

        elif (event.key() == QtCore.Qt.Key_Up or
              event.key() == QtCore.Qt.Key_PageUp or
              event.key() == QtCore.Qt.Key_Left or
              event.key() == QtCore.Qt.Key_Right or
              event.key() == QtCore.Qt.Key_Backspace):
            self.SearchInput.setFocus()


    def launchSelected(self):
        if len(self.results) < 1:
            return

        selected = self.results[self.ResultsList.currentRow()]
        if not selected:
            return

        file: string = selected['file']

        if re.match('.*\.desktop$', file):
            command = re.sub(r" %.", "", DesktopEntry(file).getExec()).strip().split(" ")
            print(f'launching "{command}" from {file}')
        else:
            command = [file]
            print(f'launching {file}')

        try:
            subprocess.Popen(command)
        except Exception as e:
            print('... failed')
        else:
            update_history(file)
            self.close()


    def on_text_changed(self):
        # Don't update the list on every textChanged event
        self.timer.start(200) # ms


    def update_results_list(self):
        self.ResultsList.clear()
        query: str = re.escape(self.SearchInput.text())
        if len(query) < 2:
            return

        results: List = []

        # Find matching desktop file
        for filename in self.find_desktop_files(query):
            item = self.find_item(filename, query)
            if item and item['score'] > 0:
                item['score'] += self.get_score_from_history(item['file'])
                results.append(item)

        # Find matching executable in $PATH
        for path in self.path_list:
            for filename in glob.glob(os.path.join(path, f'*{query}*')):
                item = find_executable(filename, query)
                if item and item['score'] > 0:
                    item['score'] += self.get_score_from_history(item['file'])
                    results.append(item)

        # Sort the results and display them in the list
        results = sorted(results, key=lambda k: k['score'], reverse=True)
        results = results[0:self.max_results_to_show]
        self.ResultsList.clear()
        for res in results:
            if res['icon'] in self.icon_cache:
                icon = self.icon_cache[res['icon']]
            else:
                if res['icon'] and QtGui.QIcon.hasThemeIcon(res['icon']):
                    icon = QtGui.QIcon.fromTheme(res['icon'])
                else:
                    icon = QtGui.QIcon.fromTheme('unknownapp')
                self.icon_cache[res['icon']] = icon

            self.ResultsList.addItem(
                QtWidgets.QListWidgetItem(
                    icon,
                    f"{res['name']}\n{res['comment']}"
                )
            )

        # Set the results to a member variable so it can be accessed by
        # launchSelected() later
        self.results = results

        # Select the first item in the list so it can be executed directly
        # when pressing return
        self.ResultsList.setCurrentRow(0)


    # param query: text string to search for in the files
    # return: List of desktop-files matching 'query' in the name, exec or
    # comment fields
    def find_desktop_files(self, query: str) -> List:
        matching_files = []
        regex = re.compile(f'^(Name|Exec|Comment)=.*{query}.*', re.I|re.M)
        for pattern in self.desktop_file_globs:
            for filename in glob.glob(pattern):
                with open(filename, "r") as f:
                    data = f.read()
                    if regex.findall(data):
                        matching_files.append(filename)

        return matching_files


    # return: How many times the file has been launched
    def get_score_from_history(self, file: str) -> int:
        if file in self.score_cache:
            return self.score_cache[file]

        if not os.path.isfile(DB_FILE) or not os.access(DB_FILE, os.R_OK):
            return 0

        try:
            conn = sqlite3.connect(DB_FILE)
            c = conn.cursor()
            res = c.execute('SELECT score FROM history WHERE file = ?', [file])
            row = res.fetchone()
            if row:
                self.score_cache[file] = row[0]
                return row[0]
        except Exception as e:
            print("Error: Could not read history file: "+DB_FILE)

        self.score_cache[file] = 0
        return 0


    # param filename: full path to the file to search in
    # param query: text string to search for in the file
    # return: if not hidden and query found in name, exec or comment,
    # return dict, otherwise return None
    def find_item(self, filename: str, query: str) -> Union[Dict, None]:
        if DesktopEntry(filename).getNoDisplay():
            return None

        if filename in self.item_cache:
            return self.item_cache[filename]

        score: int = 0
        found_name: Union[str, None] = None
        found_exec: Union[str, None] = None
        found_comment: Union[str, None] = None

        desktop_file_name: str = DesktopEntry(filename).getName()
        desktop_file_exec: str = DesktopEntry(filename).getExec()
        desktop_file_comment: str = DesktopEntry(filename).getComment()

        _score: int = calc_score_for_name(desktop_file_name, query)
        if _score > 0:
            found_name = desktop_file_name
            score += _score

        _score = calc_score_for_exec(desktop_file_exec, query)
        if _score > 0:
            found_exec = desktop_file_exec
            score += _score

        _score = calc_score_for_comment(desktop_file_comment, query)
        if _score > 0:
            found_comment = desktop_file_comment
            score += _score

        if score < 1:
            return None

        # Display the name as "name" if it was found, otherwise the executable
        # file name. If none was found, don't display this entry at all.
        name: str = ''
        if found_name:
            name = found_name
        elif found_exec:
            name = found_exec
        elif found_comment:
            name = desktop_file_name
        else:
            return None

        # Display the comment as "comment" if it was found, otherwise display the 
        # executable file name
        comment: str = ''
        if desktop_file_comment:
            comment = desktop_file_comment
        elif found_exec:
            comment = found_exec

        self.item_cache[filename] = {
            'name':     name,
            'comment':  comment,
            'icon':     DesktopEntry(filename).getIcon(),
            'score':    score,
            'file':     filename,
        }

        return self.item_cache[filename]


# end of class


# param filename: full path to the file to search in
# param query: text string to search for in the file
# return: If query is found in the files name and the file is executable,
# return dict, otherwise return None
def find_executable(filename: str, query: str) -> Union[Dict, None]:
    if not is_exe(filename) or is_backup_file(filename):
        return None

    base_filename: str = os.path.basename(filename)
    return {
        'name':     base_filename,
        'comment':  filename,
        'icon':     None,
        'score':    calc_score_for_exe_file(base_filename, query),
        'file':     filename,
    }


# return: How well the query matched the file name
def calc_score_for_exe_file(filename: str, query: str) -> int:
    score: int = 0
    if re.match(f'^{query}$', filename, re.I):
        score = 5
    elif re.match(f'^{query}.*$', filename, re.I):
        score = 3
    elif re.match(f'^.*{query}.*$', filename, re.I):
        score = 1

    return score


# return: How well the query matched the name
def calc_score_for_name(name: str, query: str) -> int:
    score: int = 0
    if re.match(f'^{query}$', name, re.I):
        score = 21
    elif re.match(f'^{query}.*$', name, re.I):
        score = 11
    elif re.match(f'^(.* |){query}(.* |)$', name, re.I):
        # name matches somewhere, whole word
        score = 8
    elif re.match(f'^.*{query}.*$', name, re.I):
        # name matches somewhere, part of word
        score = 6

    return score


# return: How well the query matched the exec-string
def calc_score_for_exec(exec: str, query: str) -> int:
    score: int = 0
    if re.match(f'^{query}( .*|)$', exec, re.I):
        # command name matches entirely, incl path
        score += 10
    elif re.match(f'^.*\/{query}$', exec, re.I):
        # command name matches, excl path
        score += 8
    elif re.match(f'^.*{query}.*$', exec, re.I):
        score += 1

    return score


# return: How well the query matched the comment
def calc_score_for_comment(comment: str, query: str) -> int:
    score: int = 0
    if re.match(f'^.*{query}.*$', comment, re.I):
        score += 1
    return score


# Save history in a sqlite file. Saves the name of the file that has been
# launched and how many time the file has been launched.
def update_history(file: str):
    try:
        os.makedirs(os.path.dirname(DB_FILE), 0o777, True)
    except Exception as e:
        print("Error: Could not create directory for history file: "+DB_FILE)
        return

    try:
        conn = sqlite3.connect(DB_FILE)
    except Exception as e:
        print("Error: Could not update history file: "+DB_FILE)
        return

    c = conn.cursor()
    c.execute(
        'CREATE TABLE IF NOT EXISTS history (file text, score int, last_used text)'
    )

    res = c.execute('SELECT score FROM history WHERE file = ?', [file])
    row = res.fetchone()
    if row:
        new_score = row[0] + 1
        c.execute(
            'UPDATE history SET score = ?, last_used = datetime("now") WHERE file = ?',
            [new_score, file]
        )
    else:
        c.execute(
            'INSERT INTO history VALUES (?, 1, datetime("now"))',
            [file]
        )

    # TODO: remove entries older than x days?

    conn.commit()


# return: True if the file ends in '~', otherwise false
def is_backup_file(filename: str) -> bool:
    return filename.endswith('~')


# return: True if the file exists and is executable, otherwise false
def is_exe(fpath: str) -> bool:
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()

    # This lets the user press CTRL-C to kill the program even when it sleeps
    timer = QTimer()
    timer.timeout.connect(lambda: None) # type: ignore
    timer.start(100)

    sys.exit(app.exec_())
