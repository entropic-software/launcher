# launcher.py
*(name subject to change)*

An application launcher for Unix-like systems. Aimed mainly at people who
just runs a window manager without a full desktop environment.

Will search for desktop-files (primarily) and executables in your $PATH
(secondarily), and sort them according to black magick.


## Screenshot

![Screenshot](screenshot.png)

## Requirements

Python 3.6, PyQt5, PyXDG

Install requirements on Debian with the command:
```apt install python3-pyqt5 python3-xdg```


## TODO

- Some kind of feedback if execution failed


## Author

Created by Tomas Åkesson <tomas@entropic.se>

See LICENSE for license details.

Project homepage: https://gitlab.com/entropic-software/launcher
